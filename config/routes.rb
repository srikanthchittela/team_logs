Rails.application.routes.draw do

  resources :system_settings
  resources :teams
  resources :team_efforts
  resources :users

  resources :sessions, only: [:new, :create, :destroy]
  match 'login', :to => 'sessions#new', :via => [:get, :post]
  match 'logout', :to => 'sessions#destroy', :via => [:get, :post]

  root 'sessions#new'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
