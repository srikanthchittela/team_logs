class AddNameToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :name, :string
    add_column :users, :employee_code, :string
    add_column :users, :team_id, :integer
    add_column :users, :user_role, :string
    add_column :users, :designation, :string
    add_column :users, :is_active, :boolean, :default => true
  end
end
