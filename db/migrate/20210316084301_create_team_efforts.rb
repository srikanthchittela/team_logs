class CreateTeamEfforts < ActiveRecord::Migration[5.2]
  def change
    create_table :team_efforts do |t|
      t.string :name
      t.integer :user_id
      t.string :jira_id
      t.text :description
      t.date :log_date
      t.string :time_slot
      t.string :comment

      t.timestamps
    end
  end
end
