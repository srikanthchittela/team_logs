class TeamEffortsController < ApplicationController
  before_action :set_team_effort, only: %i[ show edit update destroy ]
  include ApplicationHelper
  # GET /team_efforts or /team_efforts.json
  def index
    @team_efforts = TeamEffort.all
  end

  # GET /team_efforts/1 or /team_efforts/1.json
  def show
  end

  # GET /team_efforts/new
  def new
    @team_effort = TeamEffort.new
    current_time = Time.now.utc.in_time_zone('Kolkata')
    @team_effort.log_date = params[:date].blank? ? current_time.to_date : Date.parse(params[:date])
    time_slots.each_with_index do |time_slot, index|
      if !time_slot.eql?("IN") && !time_slot.eql?("OUT") && !time_slot.eql?("Train-T")
        time_intervals = time_slot.split(' - ')
        start_time = DateTime.parse(time_intervals[0]+':00', current_time.to_date)
        end_time = DateTime.parse(time_intervals[1]+':00', current_time.to_date)
        formatted_time = DateTime.parse(current_time.strftime("%H:%M"), current_time.to_date)
        if formatted_time>=start_time-15.minutes && formatted_time<end_time+15.minutes
          @team_effort.time_slot=get_recent_time_slot(time_slot, index, time_slots)
          break
        end
      end
    end
  end

  # GET /team_efforts/1/edit
  def edit
  end

  # POST /team_efforts or /team_efforts.json
  def create
    @team_effort = TeamEffort.new(team_effort_params)
    @team_effort.user_id=current_user.id
    @team_effort.name=current_user.name
    if params[:team_effort][:time_slot].eql?('OUT')
      @team_effort.errors[:jira_id] = 'Please enter LOW or MEDIUM or HIGH.' if !params[:team_effort][:jira_id].eql?('LOW') && !params[:team_effort][:jira_id].eql?('MEDIUM') && !params[:team_effort][:jira_id].eql?('HIGH')
    end
    respond_to do |format|
      if ((@team_effort.errors.blank? && @team_effort.save))
        format.html { redirect_to "/team_efforts/new#new_effort", notice: '' }
      else
        format.html { render action: "new" }
      end
    end
  end


  def create1
    @team_effort = TeamEffort.new(params[:team_effort])
    @team_effort.user_id=current_user.id
    @team_effort.name=current_user.display_name
    if params[:team_effort][:time_slot].eql?('Train-T')
      @team_effort.errors[:jira_id] = 'Please enter decimal Value .' if params[:team_effort][:jira_id].to_d<= 0.0
    elsif params[:team_effort][:time_slot].eql?('OUT')
      @team_effort.errors[:jira_id] = 'Please enter LOW or MEDIUM or HIGH.' if !params[:team_effort][:jira_id].eql?('LOW') && !params[:team_effort][:jira_id].eql?('MEDIUM') && !params[:team_effort][:jira_id].eql?('HIGH')
    end
    puts @team_effort.errors
    respond_to do |format|
      if ((@team_effort.errors.blank? && @team_effort.save))
        format.html { redirect_to "/team_efforts/new#new_effort", notice: '' }
        format.json { render json: @team_effort, status: :created, location: @team_effort }
      else
        format.html { render action: "new" }
        format.json { render json: @team_effort.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /team_efforts/1 or /team_efforts/1.json
  def update
    respond_to do |format|
      if @team_effort.update(team_effort_params)
        format.html { redirect_to @team_effort, notice: "Team effort was successfully updated." }
        format.json { render :show, status: :ok, location: @team_effort }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @team_effort.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /team_efforts/1 or /team_efforts/1.json
  def destroy
    @team_effort.destroy
    respond_to do |format|
      format.html { redirect_to team_efforts_url, notice: "Team effort was successfully destroyed." }
      format.json { head :no_content }
    end
  end



  def get_recent_time_slot(time_slot, index, time_slots)
    result=''
    if !TeamEffort.find_by_time_slot_and_user_id_and_log_date(time_slot, current_user.id, Time.now.utc.in_time_zone('Kolkata').to_date).blank?
      result=get_recent_time_slot(time_slots[index+1], index+1, time_slots) if index+1<time_slots.size
    else
      result=time_slot
    end
    result
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_team_effort
      @team_effort = TeamEffort.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def team_effort_params
      params.require(:team_effort).permit(:name, :user_id, :jira_id, :description, :log_date, :time_slot, :comment)
    end
end
