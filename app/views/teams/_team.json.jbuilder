json.extract! team, :id, :name, :shot_code, :is_active, :created_at, :updated_at
json.url team_url(team, format: :json)
