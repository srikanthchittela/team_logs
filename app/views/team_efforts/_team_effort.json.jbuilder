json.extract! team_effort, :id, :name, :user_id, :jira_id, :description, :log_date, :time_slot, :comment, :created_at, :updated_at
json.url team_effort_url(team_effort, format: :json)
