json.extract! system_setting, :id, :key, :value, :created_at, :updated_at
json.url system_setting_url(system_setting, format: :json)
