module ApplicationHelper


  def get_index_text(value)
    "<div class=\"row\">
    <div class=\"col-sm-12\" style=\"padding-left: 0px;\">
    <div class=\"head_style\">" "#{value}</div></div></div>".html_safe
  end

  def time_slots
    ["IN", "06 - 07", "07 - 08", "08 - 09", "09 - 10", "10 - 11", "11 - 12", "12 - 13", "13 - 14", "14 - 15", "15 - 16", "16 - 17", "17 - 18", "18 - 19", "19 - 20", "20 - 21", "21 - 22", "22 - 23", "23 - 00", "00 - 01", "01 - 02", "02 - 03", "03 - 04", "04 - 05", "05 - 06", "Train-T", "OUT"]
  end

  def simple_format(text)
    text = text.to_s.dup
    text.gsub!(/\r\n/, "\n") # \r\n and \r => \n
    #text.gsub!(/([^\n]\n)(?=[^\n])/, '\1<br/>')  # 1 newline   => br
    text.gsub!(/\n/, '<br/>') # 1 newline   => br
    text.gsub!(/\r/, '<br/>') # 1 newline   => br
    text.gsub!(/\t/, '&nbsp;&nbsp;&nbsp;&nbsp;') # 1 newline   => br
    text.gsub!('  ', '&nbsp;&nbsp;')
    text.html_safe
  end

end
