require "application_system_test_case"

class TeamEffortsTest < ApplicationSystemTestCase
  setup do
    @team_effort = team_efforts(:one)
  end

  test "visiting the index" do
    visit team_efforts_url
    assert_selector "h1", text: "Team Efforts"
  end

  test "creating a Team effort" do
    visit team_efforts_url
    click_on "New Team Effort"

    fill_in "Comment", with: @team_effort.comment
    fill_in "Description", with: @team_effort.description
    fill_in "Jira", with: @team_effort.jira_id
    fill_in "Log date", with: @team_effort.log_date
    fill_in "Name", with: @team_effort.name
    fill_in "Time slot", with: @team_effort.time_slot
    fill_in "User", with: @team_effort.user_id
    click_on "Create Team effort"

    assert_text "Team effort was successfully created"
    click_on "Back"
  end

  test "updating a Team effort" do
    visit team_efforts_url
    click_on "Edit", match: :first

    fill_in "Comment", with: @team_effort.comment
    fill_in "Description", with: @team_effort.description
    fill_in "Jira", with: @team_effort.jira_id
    fill_in "Log date", with: @team_effort.log_date
    fill_in "Name", with: @team_effort.name
    fill_in "Time slot", with: @team_effort.time_slot
    fill_in "User", with: @team_effort.user_id
    click_on "Update Team effort"

    assert_text "Team effort was successfully updated"
    click_on "Back"
  end

  test "destroying a Team effort" do
    visit team_efforts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Team effort was successfully destroyed"
  end
end
