require 'test_helper'

class TeamEffortsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @team_effort = team_efforts(:one)
  end

  test "should get index" do
    get team_efforts_url
    assert_response :success
  end

  test "should get new" do
    get new_team_effort_url
    assert_response :success
  end

  test "should create team_effort" do
    assert_difference('TeamEffort.count') do
      post team_efforts_url, params: { team_effort: { comment: @team_effort.comment, description: @team_effort.description, jira_id: @team_effort.jira_id, log_date: @team_effort.log_date, name: @team_effort.name, time_slot: @team_effort.time_slot, user_id: @team_effort.user_id } }
    end

    assert_redirected_to team_effort_url(TeamEffort.last)
  end

  test "should show team_effort" do
    get team_effort_url(@team_effort)
    assert_response :success
  end

  test "should get edit" do
    get edit_team_effort_url(@team_effort)
    assert_response :success
  end

  test "should update team_effort" do
    patch team_effort_url(@team_effort), params: { team_effort: { comment: @team_effort.comment, description: @team_effort.description, jira_id: @team_effort.jira_id, log_date: @team_effort.log_date, name: @team_effort.name, time_slot: @team_effort.time_slot, user_id: @team_effort.user_id } }
    assert_redirected_to team_effort_url(@team_effort)
  end

  test "should destroy team_effort" do
    assert_difference('TeamEffort.count', -1) do
      delete team_effort_url(@team_effort)
    end

    assert_redirected_to team_efforts_url
  end
end
